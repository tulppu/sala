// ==UserScript==
// @name        New script - kotchan.fun
// @namespace   Violentmonkey Scripts
// @grant       none
// @version     1.0
// @author      -
// @grant GM_addStyle
// @description 15.8.2022 22.36.12
// ==/UserScript==

window.addEventListener('load', () => {
  const style = document.createElement("style");
  style.innerText = `
  #media-viewer:not(:hover) #header {
    visibility: hidden;
  }

  a#close:not(:hover) {
    filter: opacity(40%);
  }
  `;
  document.head.appendChild(style);

  const mediaViewer = document.createElement("div");

  mediaViewer.id = "media-viewer";
  //mediaViewer.style.paddingLeft  = "1px";
  mediaViewer.style.position    = "fixed";
  mediaViewer.style.left        = "10px";
  mediaViewer.style.top         = "10px";
  mediaViewer.style.zIndex      = 100;
  mediaViewer.style.display     = "none";

  mediaViewer.overlay = document.createElement("div");
  mediaViewer.overlay.id = "overlay"
  mediaViewer.overlay.style.position  = "relative";
  mediaViewer.overlay.style.width     = "100%";
  mediaViewer.overlay.style.height    = "100%";
  mediaViewer.appendChild(mediaViewer.overlay);

  mediaViewer.overlay.header = document.createElement("div");
  mediaViewer.overlay.header.id = "header";
  mediaViewer.overlay.header.style.position   = "absolute"
  mediaViewer.overlay.header.style.width      = "100%";
  mediaViewer.overlay.header.style.top        = "0";
  mediaViewer.overlay.header.style.left       = "0";
  mediaViewer.overlay.header.style.background = "#0000003d";
  mediaViewer.overlay.header.style.height     = "25px";
  mediaViewer.overlay.appendChild(mediaViewer.overlay.header);

  mediaViewer.overlay.header.closeLink = document.createElement("a");
  mediaViewer.overlay.header.closeLink.id = "close"
  mediaViewer.overlay.header.closeLink.innerText = "âœ–";
  mediaViewer.overlay.header.closeLink.style.position     = "absolute";
  mediaViewer.overlay.header.closeLink.style.right        = "3px";
  mediaViewer.overlay.header.closeLink.style.top          = "5px";
  mediaViewer.overlay.header.closeLink.style.marginRight  = "5px";
  mediaViewer.overlay.header.closeLink.style.textShadow   = "1px 1px black";
  mediaViewer.overlay.header.closeLink.style.color		  = "rgb(243, 11, 11)";

  mediaViewer.overlay.header.closeLink.addEventListener("click", () => mediaViewer.overlay.media.clear() );
  mediaViewer.overlay.header.appendChild(mediaViewer.overlay.header.closeLink);

  mediaViewer.overlay.media = document.createElement("div");
  mediaViewer.overlay.media.id = "media";
  mediaViewer.overlay.media.get = () => mediaViewer.overlay.media.querySelector("video,audio");
  mediaViewer.overlay.media.clear = (() => {
    mediaViewer.style.display = "none";
    const media = mediaViewer.overlay.media.get();
    if (!media) return;
    media.src = null;
    media.load();
    media.parentNode.removeChild(media);
  });
  mediaViewer.overlay.appendChild(mediaViewer.overlay.media);
  document.body.appendChild(mediaViewer);


  document.addEventListener('auxclick', (e) => {
    const target = e.target;
    //if (!target || target.classList.contains(".chat_img")) return;

    const chatImgCnt = target.closest("a.chat_img_cont");
    if (!chatImgCnt) return;

    const video =  document.querySelector("video:last-of-type");
    if (!video || video.paused) return;

    const clonedVideo = video.cloneNode(video);
    clonedVideo.autoPlay = true;
    clonedVideo.currentTime = video.currentTime;
    clonedVideo.volume = video.volume;
    clonedVideo.setAttribute("controls","controls");
    clonedVideo.removeAttribute("style");
    clonedVideo.style.pointerEvents = "auto";
    clonedVideo.style.objectFit = "fill";
    clonedVideo.width = video.clientWidth;
    clonedVideo.height= video.clientHeight;

    clonedVideo.addEventListener("fullscreenchange", (e) => {
      const vid = e.target;
      if (document.fullscreenElement === vid) {
        vid.style.objectFit = "contain";
      } else {
        vid.style.objectFit = "fill";
      }
    });

    const rect = video.getBoundingClientRect();

    mediaViewer.style.left = rect.left + "px";
    mediaViewer.style.top = rect.top + "px";

    mediaViewer.overlay.media.clear()

    mediaViewer.overlay.media.appendChild(clonedVideo)
    mediaViewer.style.display = "block";

    clonedVideo.play();
    video.pause();
    video.source = null;
    video.load();
    video.style.display = "none";

    e.preventDefault();
    return false;
  });

  mediaViewer.onDrag = false;
  const prevPos = {
    x: null,
    y: null,
  };

  function mediaMover(e) {
    if (!mediaViewer.onDrag) return
    if (prevPos.x === null || prevPos.y === null) {
        prevPos.x = e.clientX;
        prevPos.y = e.clientY;
        return;
    }

    const currX = e.clientX;
    const currY = e.clientY;

    const currPos = {
      left: parseInt(getComputedStyle(mediaViewer).getPropertyValue("left")),
      top: parseInt(getComputedStyle(mediaViewer).getPropertyValue("top")),
    };

    mediaViewer.style.left = (Math.max(Math.min(currPos.left + (currX - prevPos.x), window.innerWidth - mediaViewer.clientWidth), 0)) + "px";
    mediaViewer.style.top =  (Math.max(Math.min(currPos.top + (currY - prevPos.y), window.innerHeight - mediaViewer.clientHeight), 0)) + "px";

    prevPos.x = e.clientX;
    prevPos.y = e.clientY;
  }

  document.addEventListener("mousedown", function(e) {
    if (e.which !== 1) return;
    if (!mediaViewer.overlay.media.get()) return;
    if (!mediaViewer.matches(":hover")) return;

    e.preventDefault();
    mediaViewer.onDrag = true;
    document.addEventListener("mousemove", mediaMover);

    const media = mediaViewer.overlay.media.get();
    media.style.pointerEvents = "none";

    document.addEventListener("mouseup", function(e) {
      //if (!mediaViewer.onDrag)
      if (e.which !== 1) return;
      e.preventDefault();
      mediaViewer.onDrag = false;
      document.removeEventListener("mousemove", mediaMover);
      media.style.pointerEvents = "auto";

      prevPos.x = prevPos.y = null;

    }, {once: true});
  });


  // viewer resizer
  $("#media-viewer").on("wheel", "video", (event) => {
    const media = mediaViewer.overlay.media.get();
    if (!media) return;

	const do_shrink = event.originalEvent.deltaY > 0;

  	const w = media.width;
	const h = media.height;

    const adjusted_w = (w / 100) * ((do_shrink) ? 95 : 105);
	const adjusted_h = (h / 100) * ((do_shrink) ? 95 : 105);

    const oldLeft = parseInt(getComputedStyle(mediaViewer).getPropertyValue("left"));
    mediaViewer.style.left = (oldLeft + (media.width - adjusted_w) / 2) + "px";

    media.width = adjusted_w;
    media.height = adjusted_h;
  });
});


// https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop
window.addEventListener("dragover", (e) => {
	e.preventDefault();
});


// https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop
window.addEventListener("drop", async (ev) => {
	ev.preventDefault();
	if (ev.dataTransfer.items) {
	const fileInput = document.querySelector("input#image");
    fileInput.files = ev.dataTransfer.files;;
  }
	return false;
});

