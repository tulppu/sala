#!/bin/python3

# Extracts frames from animted media within selected time range and interval and writes them to selected directory as .jpg files.
# Useful for choosing thumbnail picture for example.

from os import path
import sys
import subprocess

if len(sys.argv) == 1:
    #script_name = sys.argv[0].split("/")[-1]
    script_name = path.basename(__file__)
    print("Usage: [filename:str] [start_time:int:?] [interval:float:?] [end_time:int:?] [out_dir:str:?]\nExample: {0} ./example.gif 0 0.3 5 /tmp/".format(script_name))
    exit(0)

def ask_or_default(promt, default, casted_type=int):
    return input("{0}: ({1})\n".format(promt, str(default))) or default

def get_nth_arg(nth, casted_type=int):
    return casted_type(sys.argv[nth]) if len(sys.argv) > nth else None

file_path:str = get_nth_arg(1, str)
if not file_path or not path.isfile(file_path):
    print("Invalid input file '{}'!".format(file_path))
    exit(0); 

start:int       = get_nth_arg(2, int)
if start is None:
    start = int(ask_or_default("Start time", 0))

# Rest inputs should not be 0 in any case.
interval:float  = get_nth_arg(3, float) or float(ask_or_default("Interval", 0.5))
end:int         = get_nth_arg(4, int)   or int(ask_or_default("End time", start + 3))
out_dir:str     = get_nth_arg(5, str)   or ask_or_default("Output dir", path.dirname(file_path))

if not out_dir or not path.exists(out_dir):
    print("Invalid output dir {}".format(out_dir))
    exit(0)


frame_time = start
while frame_time < end + interval:
    out_file = path.join(out_dir, path.basename(file_path) + "." +  str(round(frame_time, 2)) + ".jpg") 
    cmd = "ffmpeg -y -ss {time} -i {in_path} -frames 1 -q:v 1 {out_file} -loglevel quiet".format(time=frame_time, in_path=file_path, out_file=out_file)
    print(out_file)
    subprocess.Popen(cmd, shell=True)
    frame_time = frame_time + interval
