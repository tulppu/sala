#!/bin/bash
# Alias/shortcut to quickly re-encode phone recorded .mp4 files to significantly reduce their file sizes by using ffmpeg with hardware acceleration provided by cuda and nvenc.
# https://docs.nvidia.com/video-technologies/video-codec-sdk/11.1/ffmpeg-with-nvidia-gpu/index.html

if ! [ -f "$1" ]; then
	echo "No such file as '$1'"
	exit 1
fi

ffmpeg -y -noautorotate -vsync 0 -hwaccel cuda -hwaccel_output_format cuda -i $1 -c:a copy -c:v h264_nvenc -b:v 5M $1.nvencd.mp4

