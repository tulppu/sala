set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

"Plugin 'preservim/nerdtree'
"Plugin 'neoclide/coc.nvim', {'branch': 'release'}
"Plugin 'mhinz/vim-grepper'
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'
Plugin 'wookayin/fzf-ripgrep.vim'
"Plugin 'tpope/vim-fugitive'
"Plugin  'ctrlpvim/ctrlp.vim'
call vundle#end()
filetype plugin indent on    " required

:set number
:syntax on
:color desert
:set cursorline
:hi CursorLine term=bold cterm=bold guibg=Grey40
:set list
:set listchars=tab:␉\ 
:set tabstop=4
:set shiftwidth=4
:set encoding=utf-8
:set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.o,*.gch,buid/*,node_modules/*

"nnoremap <C-R> :Rg<CR>
nnoremap <C-R> :Rg<CR>
nnoremap <C-P> :GFiles<CR>
nnoremap <C-H> :History<CR>
nnoremap <C-T> :Tags<CR>

" git grep https://github.com/junegunn/fzf.vim
command! -bang -nargs=* GGrep
  "\ call fzf#vim#grep(
  \ call fzf#vim#grep(
  \   'git grep --line-number -- '.fzf#shellescape(<q-args>),
  \   fzf#vim#with_preview({'dir': systemlist('git rev-parse --show-toplevel')[0]}), <bang>0)

"autocmd VimEnter * NERDTree
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif
nnoremap <C-N> :NERDTree<CR>

" AutoCompleteMe
set rtp+=~/.vim/bundle/YouCompleteMe
let MY_YCM_HIGHLIGHT_GROUP = {
      \   'typeParameter': 'PreProc',
      \   'parameter': 'Normal',
      \   'variable': 'Normal',
      \   'property': 'Normal',
      \   'enumMember': 'Normal',
      \   'event': 'Special',
      \   'member': 'Normal',
      \   'method': 'Normal',
      \   'class': 'Special',
      \   'namespace': 'Special',
      \ }

for tokenType in keys( MY_YCM_HIGHLIGHT_GROUP )
  call prop_type_add( 'YCM_HL_' . tokenType,
                    \ { 'highlight': MY_YCM_HIGHLIGHT_GROUP[ tokenType ] } )
endfor
let g:ycm_auto_trigger = 1
let g:ycm_enable_inlay_hints = 0
let g:ycm_enable_semantic_highlighting= 1

let g:ctrlp_custom_ignore = 'build\|node_modules\|DS_Store\|git|.git|*.o|*.a'
let g:ctrlp_working_path_mode = '0'


augroup RestoreCursor
  autocmd!
  autocmd BufReadPost *
    \ let line = line("'\"")
    \ | if line >= 1 && line <= line("$") && &filetype !~# 'commit'
    \      && index(['xxd', 'gitrebase'], &filetype) == -1
    \ |   execute "normal! g`\""
    \ | endif
augroup END

