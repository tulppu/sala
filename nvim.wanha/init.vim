set nocompatible              " be iMproved, required
filetype off                  " required


call plug#begin()
" Plug 'VundleVim/Vundle.vim'

Plug 'preservim/nerdtree'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'wookayin/fzf-ripgrep.vim'
Plug 'nvim-tree/nvim-web-devicons' " Recommended (for coloured icons)
Plug 'akinsho/bufferline.nvim', { 'tag': '*' }
Plug 'lewis6991/gitsigns.nvim' " OPTIONAL: for git status
Plug 'romgrk/barbar.nvim'

Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'

Plug 'vuejs/eslint-plugin-vue'
Plug 'posva/vim-vue'

Plug 'rakr/vim-one'
"Plug 'rmagatti/goto-preview'
"Plugin 'tpope/vim-fugitive'
"Plugin  'ctrlpvim/ctrlp.vim'
call plug#end()
filetype plugin indent on    " required
":lua require('goto-preview').setup { default_mapping=true }

:let g:session_autosave="yes"
:let g:session_autoload="yes"

:set clipboard=unnamedplus
:set number
:syntax on
:color koehler 
":color desert
:set cursorline
:hi CursorLine term=bold cterm=bold guibg=Grey40
:set list
:set listchars=tab:␉\ 
:set tabstop=4
:set shiftwidth=4
:set encoding=utf-8
:set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.o,*.gch,buid/*,node_modules/*

nnoremap <SPACE> <Nop>
let mapleader=" "

"nnoremap <silent>    <A-,> <Cmd>BufferPrevious<CR>
"nnoremap <silent>    <A-.> <Cmd>BufferNext<CR>

nnoremap <S-Left> <Nop>
noremap <S-Left> <Cmd>BufferPrevious<CR>
nnoremap <leader><Left> <Cmd>BufferPrevious<CR>
nnoremap <leader><S-Left> <Cmd>BufferMovePrevious<CR>
nnoremap <C-S-Left> <Cmd>BufferMovePrevious<CR>

nnoremap <S-Right> <Nop>
nnoremap <S-Right> <Cmd>BufferNext<CR>
nnoremap <leader><Right> <Cmd>BufferNext<CR>
nnoremap <leader><S-Right> <Cmd>BufferMoveNext<CR>
nnoremap <C-S-Right> <Cmd>BufferMoveNext<CR>

nnoremap <silent> <A-Right> <c-w>l
nnoremap <silent> <A-Left> <c-w>h
nnoremap <silent> <A-Up> <c-w>k
nnoremap <silent> <A-Down> <c-w>j

"nnoremap <C-R> :Rg<CR>
nnoremap <C-R> :Rg<CR>
nnoremap <leader>rn <Plug>(coc-rename) 
nnoremap <S>rn <Plug>(coc-rename) 
nnoremap <C-P> :GFiles<CR>
nnoremap <C-H> :History<CR>
nnoremap <C-T> :Tags<CR>
nnoremap <C-B> :Buffer<CR> 
"nnoremap <C-D> :call CocAction('jumpDefinition')<CR> 
"nnoremap <g><d> :call CocAction('jumpDeclaration')<CR> 

nnoremap <leader>l :CocList<CR>
"nnoremap <C-B> <Nop>
"nnoremap <C-B> :bd<CR> 

"autocmd VimEnter * NERDTree
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif
nnoremap <C-N> :NERDTreeToggle<CR>


nnoremap  ,v :edit   $MYVIMRC<CR>


let g:ctrlp_custom_ignore = 'build\|node_modules\|DS_Store\|git|.git|*.o|*.a'
let g:ctrlp_working_path_mode = '0'

" autocmd VimEnter * CocCommand session.load default

"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

autocmd CursorHold * silent call CocActionAsync('highlight')
colorscheme one
set background=dark " for the light version
let g:one_allow_italics = 1 " I love italic for comments

augroup RestoreCursor
  autocmd!
  autocmd BufReadPost *
    \ let line = line("'\"")
    \ | if line >= 1 && line <= line("$") && &filetype !~# 'commit'
    \      && index(['xxd', 'gitrebase'], &filetype) == -1
    \ |   execute "normal! g`\""
    \ | endif
augroup END


 source $HOME/.config/nvim/init.coc.lua

