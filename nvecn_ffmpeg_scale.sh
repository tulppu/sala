#!/bin/bash
# Alias/shortcut to quickly re-encode and rescale phone recorded .mp4 files to significantly reduce their file sizes by using ffmpeg with hardware acceleration provided by cuda and nvenc.
# https://docs.nvidia.com/video-technologies/video-codec-sdk/11.1/ffmpeg-with-nvidia-gpu/index.html


if ! [  "$1" ]; then
	echo "Example use: $0 VID.mp4 720:-2 1600k output.webm"
	exit 1	
fi

if ! [ -f "$1" ]; then
	echo "No such file as '$1'"
	exit 1
fi

SCALE="${2:-720:-2}"
BITRATE="${3:-800k}"
OUTPUT="${4:-"$1.nvenc.scaled.$SCALE-$BITRATE.mp4"}"

echo $OUTPUT

ffmpeg -noautorotate -hwaccel cuda -hwaccel_output_format cuda -i $1 -vf scale_cuda="$SCALE" -crf 26 -b "$BITRATE" -c:a copy -c:v h264_nvenc $OUTPUT -loglevel quiet -stats

